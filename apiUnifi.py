import argparse
from unifi.controller import Controller

c = Controller('185.44.24.16', 'admin', 't1l2cm3r', '8443', 'v3', 'emartinez')
aps = c.get_aps()
ap_names = dict([(ap['mac'], ap['name']) for ap in aps])
clients = c.get_clients()
clients.sort(key=lambda x: -x['rssi'])
print(clients)

c = Controller('185.44.24.16', 'admin', 't1l2cm3r', '8443', 'v3', 'em-oficinas-centrales')
aps = c.get_aps()
ap_names = dict([(ap['mac'], ap['name']) for ap in aps])
clients = c.get_clients()
clients.sort(key=lambda x: -x['rssi'])
print(clients)
